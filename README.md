![logo.png](https://bitbucket.org/repo/G9kKdd/images/2467693935-logo.png)

Flowsnake is a multiuser web app that allows you to create interactive illustrated texts. Readers can choose which path to follow through the story, and can pick up (invisible) tokens on the way, which will change how the story unfolds for them in the future.

Stories can be have many levels of complexity:

 - A simple linear sequence
 - A series of forking paths
 - An interconnected network of paths

The application has two main parts:

 - An **[Editor](Editor)**, where you can upload images, arrange them in branching story lines, and add tokens. You can also visualize all the different ways that a reader can have arrived at a given page.
 - A **[Reader](Reader)**, where you can choose your own path through the story, and save your progress for your next visit.

It can be used to create:

 - A simple storybook
 - An interactive graphic novel
 - Tutorials
 - Troubleshooting user guides
 - Species recognition keys

In the Editor mode, it can also be used for creating collaborative mindmaps.

See the [FlowSnake Wiki](https://bitbucket.org/team-flowsnake/flowsnake/wiki/Home) for information about this project.

### Who do I talk to? ###

* [James Newton](mailto:bitbucket@lexogram.com?Subject=FlowSnake)